# Figma Design - DesignCourse

Video Course : [UI / UX Design Tutorial – Wireframe, Mockup & Design in Figma](https://youtu.be/c9Wg6Cb_YlU)
<br>
Project Detail : [https://www.figma.com](https://www.figma.com/file/vxp2AvMouFzxw2Tz2bcQ9C/UI-UX-Design-Tutorial---Wireframe%2C-Mockup-%26-Design-Figma?node-id=0%3A1&t=nPjL7Na538MFdFOv-1)
<br>

If you want to download design project, please click [here](https://gitlab.com/maulanakurnia/a5/-/raw/main/figma/freeCodeCamp.org/01-UI%20UX%20Tutorial/UI%20UX%20Design%20Tutorial%20-%20Wireframe,%20Mockup%20&%20Design%20Figma.fig?inline=false)

## Screenshoot

### 1. Homepage

![Homepage](screenshot/ui-layout.png)

### 2. Desktop Mockup Design

![Desktop Mockup Design](screenshot/dekstop-mockup.png)

### 3. Mobile Mockup Design

![Mobile Mockup Design](screenshot/mobile-mockup.png)
