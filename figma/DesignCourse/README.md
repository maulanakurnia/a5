# Figma Design - DesignCourse

Video Course : [The Figma 2021 Crash Course by Example](https://youtu.be/Gu1so3pz4bA)
<br>
Project Detail : [https://www.figma.com](https://www.figma.com/file/HPvXggfo7h8xERycxq02rD/DesignCourse?node-id=0%3A1&t=cXTd7buJI2U0Okl8-1)
<br>
Prototype Project : [https://www.figma.com](https://www.figma.com/proto/HPvXggfo7h8xERycxq02rD/DesignCourse?page-id=0%3A1&node-id=1%3A89&viewport=526%2C362%2C0.17&scaling=scale-down&starting-point-node-id=1%3A89)

If you want to download design project, please click [here](https://gitlab.com/maulanakurnia/a5/-/raw/main/DesignCourse/DesignCourse.fig?inline=false)

## Download Asset

Background Pattern : [https://www.toptal.com](https://www.toptal.com/designers/subtlepatterns/)
<br>
SVG Ilustrator : [https://lukaszadam.com](https://lukaszadam.com/illustrations)
<br>

## Screenshoot

### 1. Homepage

![Homepage](screenshot/homepage.png)

### 2. Our Work

![Our Work](screenshot/our-work.png)

### 3. Our Work Hover Image

![Our Work Hover Image](screenshot/our-work-hovering.png)

### 4. Our Work Detail

![Our Work Hover Image](screenshot/our-work-click.png)
