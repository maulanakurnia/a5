# Create a Design System with Figma - Full Course

Video Course : [Create a Design System with Figma - Full Course](https://www.youtube.com/watch?v=RYDiDpW2VkM&feature=youtu.be)
<br>
Project Detail : [https://www.figma.com](https://www.figma.com/file/WuxkMy6EW0K3qwe94rJE1j/Design-System?node-id=0%3A1&t=q7N3B70br8kDamMf-1)
<br>

If you want to download design project, please click [here](https://gitlab.com/maulanakurnia/a5/-/raw/main/figma/freeCodeCamp.org/02-Design%20System/Design%20System.fig?inline=false)

## Screenshoot

### 1. Color

![Color](screenshot/color.png)

### 2. Typhography

![Typhography](screenshot/typography.png)

### 3. Elevation

![Elevation](screenshot/elevation.png)

### 4. Button

![Button](screenshot/button.png)

### 5. Navigation Rails

![Navigation Rails](screenshot/Navigation%20Rails.png)

### 6. Lists

![Lists](screenshot/Lists.png)

### 7. Banners

![Banners](screenshot/Banners.png)

### 8. Dropdown Menu

![Dropdown Menu](screenshot/Dropdown%20Menus.png)
